msgid ""
msgstr ""
"Project-Id-Version: phonetrack\n"
"Report-Msgid-Bugs-To: translations@owncloud.org\n"
"POT-Creation-Date: 2018-05-21 14:20+0200\n"
"PO-Revision-Date: 2018-05-21 11:57-0400\n"
"Last-Translator: eneiluj <eneiluj@posteo.net>\n"
"Language-Team: Danish\n"
"Language: da_DK\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: phonetrack\n"
"X-Crowdin-Language: da\n"
"X-Crowdin-File: /master/l10n/templates/phonetrack.pot\n"

#: app.php:41
msgid "PhoneTrack"
msgstr ""

#: logcontroller.php:207 logcontroller.php:233
msgid "Geofencing alert"
msgstr ""

#: logcontroller.php:210
#, php-format
msgid "In session \"%s\", device \"%s\" entered geofencing zone \"%s\"."
msgstr ""

#: logcontroller.php:236
#, php-format
msgid "In session \"%s\", device \"%s\" exited geofencing zone \"%s\"."
msgstr ""

#: leaflet.js:5
msgid "left"
msgstr ""

#: leaflet.js:5
msgid "right"
msgstr ""

#: phonetrack.js:544 maincontent.php:69
msgid "Show lines"
msgstr "Vis linjer"

#: phonetrack.js:552
msgid "Hide lines"
msgstr "Skjule linjer"

#: phonetrack.js:573
msgid "Activate automatic zoom"
msgstr ""

#: phonetrack.js:581
msgid "Disable automatic zoom"
msgstr ""

#: phonetrack.js:602
msgid "Show last point tooltip"
msgstr ""

#: phonetrack.js:610
msgid "Hide last point tooltip"
msgstr ""

#: phonetrack.js:631
msgid "Zoom on all devices"
msgstr "Zoom på alle enheder"

#: phonetrack.js:644
msgid "Click on the map to move the point, press ESC to cancel"
msgstr ""

#: phonetrack.js:847
msgid "Server name or server url should not be empty"
msgstr ""

#: phonetrack.js:850 phonetrack.js:859
msgid "Impossible to add tile server"
msgstr ""

#: phonetrack.js:856
msgid "A server with this name already exists"
msgstr ""

#: phonetrack.js:892 phonetrack.js:3357 maincontent.php:250 maincontent.php:294
#: maincontent.php:339 maincontent.php:388
msgid "Delete"
msgstr ""

#: phonetrack.js:925
msgid "Tile server \"{ts}\" has been added"
msgstr ""

#: phonetrack.js:928
msgid "Failed to add tile server \"{ts}\""
msgstr ""

#: phonetrack.js:932
msgid "Failed to contact server to add tile server"
msgstr ""

#: phonetrack.js:966
msgid "Tile server \"{ts}\" has been deleted"
msgstr ""

#: phonetrack.js:969
msgid "Failed to delete tile server \"{ts}\""
msgstr ""

#: phonetrack.js:973
msgid "Failed to contact server to delete tile server"
msgstr ""

#: phonetrack.js:1107
msgid "Failed to contact server to restore options values"
msgstr ""

#: phonetrack.js:1110 phonetrack.js:1193 phonetrack.js:4854
msgid "Reload this page"
msgstr ""

#: phonetrack.js:1190
msgid "Failed to contact server to save options values"
msgstr ""

#: phonetrack.js:1213
msgid "Session name should not be empty"
msgstr ""

#: phonetrack.js:1230
msgid "Session name already used"
msgstr ""

#: phonetrack.js:1234
msgid "Failed to contact server to create session"
msgstr ""

#: phonetrack.js:1329
msgid "Watch this session"
msgstr ""

#: phonetrack.js:1335
msgid "shared by {u}"
msgstr ""

#: phonetrack.js:1340 phonetrack.js:2532
msgid "More actions"
msgstr ""

#: phonetrack.js:1344
msgid "Zoom on this session"
msgstr ""

#: phonetrack.js:1347
msgid "URL to share session"
msgstr ""

#: phonetrack.js:1351
msgid "URLs for logging apps"
msgstr ""

#: phonetrack.js:1355
msgid "Reserve device names"
msgstr ""

#: phonetrack.js:1364
msgid "Delete session"
msgstr ""

#: phonetrack.js:1365 phonetrack.js:1366
msgid "Rename session"
msgstr ""

#: phonetrack.js:1368 phonetrack.js:1369
msgid "Export to gpx"
msgstr ""

#: phonetrack.js:1374
msgid "Files are created in '{exdir}'"
msgstr ""

#: phonetrack.js:1375
msgid "Automatic export"
msgstr ""

#: phonetrack.js:1377
msgid "never"
msgstr ""

#: phonetrack.js:1378
msgid "daily"
msgstr ""

#: phonetrack.js:1379
msgid "weekly"
msgstr ""

#: phonetrack.js:1380
msgid "monthly"
msgstr ""

#: phonetrack.js:1385
msgid "Automatic purge is triggered daily and will delete points older than selected duration"
msgstr ""

#: phonetrack.js:1386
msgid "Automatic purge"
msgstr ""

#: phonetrack.js:1388
msgid "don't purge"
msgstr ""

#: phonetrack.js:1389
msgid "a day"
msgstr ""

#: phonetrack.js:1390
msgid "a week"
msgstr ""

#: phonetrack.js:1391
msgid "a month"
msgstr ""

#: phonetrack.js:1400
msgid "Name reservation is optional."
msgstr ""

#: phonetrack.js:1401
msgid "Name can be set directly in logging URL if it is not reserved."
msgstr ""

#: phonetrack.js:1402
msgid "To log with a reserved name, use its token in logging URL."
msgstr ""

#: phonetrack.js:1403
msgid "If a name is reserved, the only way to log with this name is with its token."
msgstr ""

#: phonetrack.js:1406
msgid "Reserve this device name"
msgstr ""

#: phonetrack.js:1408
msgid "Type reserved name and press 'Enter'"
msgstr ""

#: phonetrack.js:1422
msgid "Share with user"
msgstr ""

#: phonetrack.js:1424
msgid "Type user name and press 'Enter'"
msgstr ""

#: phonetrack.js:1429 phonetrack.js:3899
msgid "Shared with {u}"
msgstr ""

#: phonetrack.js:1435
msgid "A private session is not visible on public browser logging page"
msgstr ""

#: phonetrack.js:1437 phonetrack.js:4863
msgid "Make session public"
msgstr ""

#: phonetrack.js:1440 phonetrack.js:4858
msgid "Make session private"
msgstr ""

#: phonetrack.js:1445
msgid "Public watch URL"
msgstr ""

#: phonetrack.js:1447
msgid "API URL (JSON last positions)"
msgstr ""

#: phonetrack.js:1453
msgid "Current active filters will be applied on shared view"
msgstr ""

#: phonetrack.js:1455
msgid "Add public filtered share"
msgstr ""

#: phonetrack.js:1465
msgid "List of server URLs to configure logging apps."
msgstr ""

#: phonetrack.js:1466 maincontent.php:182
msgid "Replace 'yourname' with the desired device name or with the name reservation token"
msgstr ""

#: phonetrack.js:1468
msgid "Public browser logging URL"
msgstr ""

#: phonetrack.js:1470
msgid "OsmAnd URL"
msgstr ""

#: phonetrack.js:1474
msgid "GpsLogger GET and POST URL"
msgstr ""

#: phonetrack.js:1478
msgid "Owntracks (HTTP mode) URL"
msgstr ""

#: phonetrack.js:1482
msgid "Ulogger URL"
msgstr ""

#: phonetrack.js:1486
msgid "Traccar URL"
msgstr ""

#: phonetrack.js:1490
msgid "OpenGTS URL"
msgstr ""

#: phonetrack.js:1493
msgid "HTTP GET URL"
msgstr ""

#: phonetrack.js:1540
msgid "The session you want to delete does not exist"
msgstr ""

#: phonetrack.js:1543
msgid "Failed to delete session"
msgstr ""

#: phonetrack.js:1547
msgid "Failed to contact server to delete session"
msgstr ""

#: phonetrack.js:1567
msgid "Device '{d}' of session '{s}' has been deleted"
msgstr ""

#: phonetrack.js:1570
msgid "Failed to delete device '{d}' of session '{s}'"
msgstr ""

#: phonetrack.js:1574
msgid "Failed to contact server to delete device"
msgstr ""

#: phonetrack.js:1626
msgid "Impossible to rename session"
msgstr ""

#: phonetrack.js:1630
msgid "Failed to contact server to rename session"
msgstr ""

#: phonetrack.js:1678
msgid "Impossible to rename device"
msgstr ""

#: phonetrack.js:1682
msgid "Failed to contact server to rename device"
msgstr ""

#: phonetrack.js:1739
msgid "Device already exists in target session"
msgstr ""

#: phonetrack.js:1742
msgid "Impossible to move device to another session"
msgstr ""

#: phonetrack.js:1746
msgid "Failed to contact server to move device"
msgstr ""

#: phonetrack.js:1813
msgid "Failed to contact server to get sessions"
msgstr ""

#: phonetrack.js:2204 phonetrack.js:2254
msgid "Stats of all points"
msgstr ""

#: phonetrack.js:2251
msgid "Stats of filtered points"
msgstr ""

#: phonetrack.js:2452
msgid "Device's color successfully changed"
msgstr ""

#: phonetrack.js:2455
msgid "Failed to save device's color"
msgstr ""

#: phonetrack.js:2459
msgid "Failed to contact server to change device's color"
msgstr ""

#: phonetrack.js:2535
msgid "Delete this device"
msgstr ""

#: phonetrack.js:2537
msgid "Rename this device"
msgstr ""

#: phonetrack.js:2540
msgid "Move to another session"
msgstr ""

#: phonetrack.js:2543 maincontent.php:48
msgid "Ok"
msgstr ""

#: phonetrack.js:2551
msgid "Device geofencing zones"
msgstr ""

#: phonetrack.js:2555
msgid "Zoom on geofencing area, then set values, then validate."
msgstr ""

#: phonetrack.js:2556 phonetrack.js:4043
msgid "URL to request when entering"
msgstr ""

#: phonetrack.js:2558 phonetrack.js:4044
msgid "URL to request when leaving"
msgstr ""

#: phonetrack.js:2561
msgid "Use current map view as geofencing zone"
msgstr ""

#: phonetrack.js:2562
msgid "Add zone"
msgstr ""

#: phonetrack.js:2574
msgid "Toggle detail/edition points"
msgstr ""

#: phonetrack.js:2582
msgid "Toggle lines"
msgstr ""

#: phonetrack.js:2593 phonetrack.js:2599
msgid "Center map on device"
msgstr ""

#: phonetrack.js:2605
msgid "Follow this device (autozoom)"
msgstr ""

#: phonetrack.js:2917
msgid "The point you want to edit does not exist or you're not allowed to edit it"
msgstr ""

#: phonetrack.js:2921
msgid "Failed to contact server to edit point"
msgstr ""

#: phonetrack.js:3054
msgid "The point you want to delete does not exist or you're not allowed to delete it"
msgstr ""

#: phonetrack.js:3058
msgid "Failed to contact server to delete point"
msgstr ""

#: phonetrack.js:3176
msgid "Impossible to add this point"
msgstr ""

#: phonetrack.js:3180
msgid "Failed to contact server to add point"
msgstr ""

#: phonetrack.js:3313
msgid "Date"
msgstr ""

#: phonetrack.js:3316
msgid "Time"
msgstr ""

#: phonetrack.js:3321 phonetrack.js:3373
msgid "Altitude"
msgstr ""

#: phonetrack.js:3324 phonetrack.js:3377
msgid "Precision"
msgstr ""

#: phonetrack.js:3327 phonetrack.js:3383
msgid "Speed"
msgstr ""

#: phonetrack.js:3335 phonetrack.js:3387
msgid "Bearing"
msgstr ""

#: phonetrack.js:3338 phonetrack.js:3391
msgid "Satellites"
msgstr ""

#: phonetrack.js:3341 phonetrack.js:3395
msgid "Battery"
msgstr ""

#: phonetrack.js:3344 phonetrack.js:3399
msgid "User-agent"
msgstr ""

#: phonetrack.js:3347
msgid "lat : lng"
msgstr ""

#: phonetrack.js:3351
msgid "DMS coords"
msgstr ""

#: phonetrack.js:3356
msgid "Save"
msgstr ""

#: phonetrack.js:3358
msgid "Move"
msgstr ""

#: phonetrack.js:3359
msgid "Cancel"
msgstr ""

#: phonetrack.js:3577
msgid "File extension must be '.gpx' to be imported"
msgstr ""

#: phonetrack.js:3595
msgid "Failed to create imported session"
msgstr ""

#: phonetrack.js:3599 phonetrack.js:3605
msgid "Failed to import session"
msgstr ""

#: phonetrack.js:3600
msgid "File is not readable"
msgstr ""

#: phonetrack.js:3606
msgid "File does not exist"
msgstr ""

#: phonetrack.js:3614
msgid "Failed to contact server to import session"
msgstr ""

#: phonetrack.js:3634
msgid "Session successfully exported in"
msgstr ""

#: phonetrack.js:3638
msgid "Failed to export session"
msgstr ""

#: phonetrack.js:3643
msgid "Failed to contact server to export session"
msgstr ""

#: phonetrack.js:3674
msgid "Failed to contact server to log position"
msgstr ""

#: phonetrack.js:3822
msgid "'{n}' is already reserved"
msgstr ""

#: phonetrack.js:3825
msgid "Failed to reserve '{n}'"
msgstr ""

#: phonetrack.js:3828
msgid "Failed to contact server to reserve device name"
msgstr ""

#: phonetrack.js:3859 phonetrack.js:3863
msgid "Failed to delete reserved name"
msgstr ""

#: phonetrack.js:3860
msgid "This device does not exist"
msgstr ""

#: phonetrack.js:3864
msgid "This device name is not reserved, please reload this page"
msgstr ""

#: phonetrack.js:3867
msgid "Failed to contact server to delete reserved name"
msgstr ""

#: phonetrack.js:3887
msgid "User does not exist"
msgstr ""

#: phonetrack.js:3890
msgid "Failed to add user share"
msgstr ""

#: phonetrack.js:3893
msgid "Failed to contact server to add user share"
msgstr ""

#: phonetrack.js:3924
msgid "Failed to delete user share"
msgstr ""

#: phonetrack.js:3927
msgid "Failed to contact server to delete user share"
msgstr ""

#: phonetrack.js:3945 phonetrack.js:3969
msgid "Public share has been successfully modified"
msgstr ""

#: phonetrack.js:3948 phonetrack.js:3972
msgid "Failed to modify public share"
msgstr ""

#: phonetrack.js:3951 phonetrack.js:3975
msgid "Failed to contact server to modify public share"
msgstr ""

#: phonetrack.js:3993
msgid "Device name restriction has been successfully set"
msgstr ""

#: phonetrack.js:3996
msgid "Failed to set public share device name restriction"
msgstr ""

#: phonetrack.js:3999
msgid "Failed to contact server to set public share device name restriction"
msgstr ""

#: phonetrack.js:4029
msgid "Warning : User email and server admin email must be set to receive geofencing alerts."
msgstr ""

#: phonetrack.js:4033
msgid "Failed to add geofencing zone"
msgstr ""

#: phonetrack.js:4036
msgid "Failed to contact server to add geofencing zone"
msgstr ""

#: phonetrack.js:4072
msgid "Failed to delete geofencing zone"
msgstr ""

#: phonetrack.js:4075
msgid "Failed to contact server to delete geofencing zone"
msgstr ""

#: phonetrack.js:4094
msgid "Failed to add public share"
msgstr ""

#: phonetrack.js:4097
msgid "Failed to contact server to add public share"
msgstr ""

#: phonetrack.js:4116
msgid "Show this device only"
msgstr ""

#: phonetrack.js:4118
msgid "Show last positions only"
msgstr ""

#: phonetrack.js:4120
msgid "Simplify positions to nearest geofencing zone center"
msgstr ""

#: phonetrack.js:4139
msgid "No filters"
msgstr ""

#: phonetrack.js:4163
msgid "Failed to delete public share"
msgstr ""

#: phonetrack.js:4166
msgid "Failed to contact server to delete public share"
msgstr ""

#: phonetrack.js:4184
msgid "Failed to contact server to get user list"
msgstr ""

#: phonetrack.js:4197
msgid "device name"
msgstr ""

#: phonetrack.js:4198
msgid "distance (km)"
msgstr ""

#: phonetrack.js:4199
msgid "duration"
msgstr ""

#: phonetrack.js:4200
msgid "#points"
msgstr ""

#: phonetrack.js:4242
msgid "years"
msgstr ""

#: phonetrack.js:4245
msgid "days"
msgstr ""

#: phonetrack.js:4264
msgid "In OsmAnd, go to 'Plugins' in the main menu, then activate 'Trip recording' plugin and go to its settings."
msgstr ""

#: phonetrack.js:4265
msgid "Copy the URL below into the 'Online tracking web address' field."
msgstr ""

#: phonetrack.js:4269
msgid "In GpsLogger, go to 'Logging details' in the sidebar menu, then activate 'Log to custom URL'."
msgstr ""

#: phonetrack.js:4270
msgid "Copy the URL below into the 'URL' field."
msgstr ""

#: phonetrack.js:4278
msgid "In Ulogger, go to settings menu and copy the URL below into the 'Server URL' field."
msgstr ""

#: phonetrack.js:4279
msgid "Set 'User name' and 'Password' mandatory fields to any value as they will be ignored by PhoneTrack."
msgstr ""

#: phonetrack.js:4280
msgid "Activate 'Live synchronization'."
msgstr ""

#: phonetrack.js:4284
msgid "In Traccar client, copy the URL below into the 'server URL' field."
msgstr ""

#: phonetrack.js:4288
msgid "You can log with any other client with a simple HTTP request."
msgstr ""

#: phonetrack.js:4289
msgid "Make sure the logging system sets values for at least 'timestamp', 'lat' and 'lon' GET parameters."
msgstr ""

#: phonetrack.js:4292
msgid "Configure {loggingApp} for logging to session '{sessionName}'"
msgstr ""

#: phonetrack.js:4423
msgid "Are you sure you want to delete the session {session} ?"
msgstr ""

#: phonetrack.js:4426
msgid "Confirm session deletion"
msgstr ""

#: phonetrack.js:4484
msgid "Choose auto export target path"
msgstr ""

#: phonetrack.js:4622
msgid "Select storage location for '{fname}'"
msgstr ""

#: phonetrack.js:4789
msgid "Are you sure you want to delete the device {device} ?"
msgstr ""

#: phonetrack.js:4792
msgid "Confirm device deletion"
msgstr ""

#: phonetrack.js:4850
msgid "Failed to toggle session public status, session does not exist"
msgstr ""

#: phonetrack.js:4853
msgid "Failed to contact server to toggle session public status"
msgstr ""

#: phonetrack.js:4886
msgid "Failed to set session auto export value"
msgstr ""

#: phonetrack.js:4887 phonetrack.js:4914
msgid "session does not exist"
msgstr ""

#: phonetrack.js:4891
msgid "Failed to contact server to set session auto export value"
msgstr ""

#: phonetrack.js:4913
msgid "Failed to set session auto purge value"
msgstr ""

#: phonetrack.js:4918
msgid "Failed to contact server to set session auto purge value"
msgstr ""

#: phonetrack.js:5001
msgid "Import gpx session file"
msgstr ""

#: maincontent.php:4
msgid "Main tab"
msgstr ""

#: maincontent.php:5
msgid "Filters"
msgstr ""

#: maincontent.php:14
msgid "Stats"
msgstr ""

#: maincontent.php:15 maincontent.php:215
msgid "Settings and extra actions"
msgstr ""

#: maincontent.php:16 maincontent.php:609
msgid "About PhoneTrack"
msgstr ""

#: maincontent.php:36
msgid "Import session"
msgstr ""

#: maincontent.php:40
msgid "Create session"
msgstr ""

#: maincontent.php:44
msgid "Session name"
msgstr ""

#: maincontent.php:57
msgid "Options"
msgstr ""

#: maincontent.php:65
msgid "Auto zoom"
msgstr ""

#: maincontent.php:73
msgid "Show tooltips"
msgstr ""

#: maincontent.php:76
msgid "Refresh each (sec)"
msgstr ""

#: maincontent.php:79
msgid "Refresh"
msgstr ""

#: maincontent.php:81 maincontent.php:86
msgid "Cutting lines only affects map view and stats table"
msgstr ""

#: maincontent.php:83
msgid "Minimum distance to cut between two points"
msgstr ""

#: maincontent.php:84
msgid "meters"
msgstr ""

#: maincontent.php:88
msgid "Minimum time to cut between two points"
msgstr ""

#: maincontent.php:89
msgid "seconds"
msgstr ""

#: maincontent.php:93
msgid "Draw line with color gradient"
msgstr ""

#: maincontent.php:97
msgid "Show accuracy in tooltips"
msgstr ""

#: maincontent.php:101
msgid "Show speed in tooltips"
msgstr ""

#: maincontent.php:105
msgid "Show bearing in tooltips"
msgstr ""

#: maincontent.php:109
msgid "Show satellites in tooltips"
msgstr ""

#: maincontent.php:113
msgid "Show battery level in tooltips"
msgstr ""

#: maincontent.php:117
msgid "Show elevation in tooltips"
msgstr ""

#: maincontent.php:121
msgid "Show user-agent in tooltips"
msgstr ""

#: maincontent.php:126
msgid "Make points draggable in edition mode"
msgstr ""

#: maincontent.php:130
msgid "Show accuracy circle on hover"
msgstr ""

#: maincontent.php:133
msgid "Line width"
msgstr ""

#: maincontent.php:137
msgid "Point radius"
msgstr ""

#: maincontent.php:141
msgid "Points and lines opacity"
msgstr ""

#: maincontent.php:145
msgid "Theme"
msgstr ""

#: maincontent.php:147
msgid "bright"
msgstr ""

#: maincontent.php:148
msgid "pastel"
msgstr ""

#: maincontent.php:149
msgid "dark"
msgstr ""

#: maincontent.php:153
msgid "Auto export path"
msgstr ""

#: maincontent.php:158
msgid "Export one file per device"
msgstr ""

#: maincontent.php:160
msgid "reload page to make changes effective"
msgstr ""

#: maincontent.php:167
msgid "Device name"
msgstr ""

#: maincontent.php:169
msgid "Log my position in this session"
msgstr ""

#: maincontent.php:174
msgid "Tracking sessions"
msgstr ""

#: maincontent.php:220
msgid "Custom tile servers"
msgstr ""

#: maincontent.php:223 maincontent.php:263 maincontent.php:306
#: maincontent.php:351
msgid "Server name"
msgstr ""

#: maincontent.php:224 maincontent.php:264 maincontent.php:307
#: maincontent.php:352
msgid "For example : my custom server"
msgstr ""

#: maincontent.php:225 maincontent.php:265 maincontent.php:308
#: maincontent.php:353
msgid "Server url"
msgstr ""

#: maincontent.php:226 maincontent.php:309
msgid "For example : http://tile.server.org/cycle/{z}/{x}/{y}.png"
msgstr ""

#: maincontent.php:227 maincontent.php:267 maincontent.php:310
#: maincontent.php:355
msgid "Min zoom (1-20)"
msgstr ""

#: maincontent.php:229 maincontent.php:269 maincontent.php:312
#: maincontent.php:357
msgid "Max zoom (1-20)"
msgstr ""

#: maincontent.php:231 maincontent.php:275 maincontent.php:320
#: maincontent.php:369
msgid "Add"
msgstr ""

#: maincontent.php:234
msgid "Your tile servers"
msgstr ""

#: maincontent.php:260
msgid "Custom overlay tile servers"
msgstr ""

#: maincontent.php:266 maincontent.php:354
msgid "For example : http://overlay.server.org/cycle/{z}/{x}/{y}.png"
msgstr ""

#: maincontent.php:271 maincontent.php:359
msgid "Transparent"
msgstr ""

#: maincontent.php:273 maincontent.php:361
msgid "Opacity (0.0-1.0)"
msgstr ""

#: maincontent.php:278
msgid "Your overlay tile servers"
msgstr ""

#: maincontent.php:303
msgid "Custom WMS tile servers"
msgstr ""

#: maincontent.php:314 maincontent.php:363
msgid "Format"
msgstr ""

#: maincontent.php:316 maincontent.php:365
msgid "WMS version"
msgstr ""

#: maincontent.php:318 maincontent.php:367
msgid "Layers to display"
msgstr ""

#: maincontent.php:323
msgid "Your WMS tile servers"
msgstr ""

#: maincontent.php:348
msgid "Custom WMS overlay servers"
msgstr ""

#: maincontent.php:372
msgid "Your WMS overlay tile servers"
msgstr ""

#: maincontent.php:401
msgid "Manually add a point"
msgstr ""

#: maincontent.php:403 maincontent.php:421
msgid "Session"
msgstr ""

#: maincontent.php:407 maincontent.php:425
msgid "Device"
msgstr ""

#: maincontent.php:409
msgid "Add a point"
msgstr ""

#: maincontent.php:410
msgid "Now, click on the map to add a point (if session is not activated, you won't see added point)"
msgstr ""

#: maincontent.php:411
msgid "Cancel add point"
msgstr ""

#: maincontent.php:416
msgid "Delete multiple points"
msgstr ""

#: maincontent.php:419
msgid "Choose a session, a device and adjust the filters. All displayed points for selected device will be deleted. An empty device name selects them all."
msgstr ""

#: maincontent.php:427
msgid "Delete points"
msgstr ""

#: maincontent.php:428
msgid "Delete only visible points"
msgstr ""

#: maincontent.php:435
msgid "Filter points"
msgstr ""

#: maincontent.php:440
msgid "Apply filters"
msgstr ""

#: maincontent.php:444
msgid "Begin date"
msgstr ""

#: maincontent.php:450 maincontent.php:469
msgid "today"
msgstr ""

#: maincontent.php:456
msgid "Begin time"
msgstr ""

#: maincontent.php:463
msgid "End date"
msgstr ""

#: maincontent.php:475
msgid "End time"
msgstr ""

#: maincontent.php:482
msgid "Min-- and Max--"
msgstr ""

#: maincontent.php:485
msgid "Min++ and Max++"
msgstr ""

#: maincontent.php:489
msgid "Last day:hour:min"
msgstr ""

#: maincontent.php:497
msgid "Minimum accuracy"
msgstr ""

#: maincontent.php:505
msgid "Maximum accuracy"
msgstr ""

#: maincontent.php:513
msgid "Minimum elevation"
msgstr ""

#: maincontent.php:521
msgid "Maximum elevation"
msgstr ""

#: maincontent.php:529
msgid "Minimum battery level"
msgstr ""

#: maincontent.php:537
msgid "Maximum battery level"
msgstr ""

#: maincontent.php:545
msgid "Minimum speed"
msgstr ""

#: maincontent.php:553
msgid "Maximum speed"
msgstr ""

#: maincontent.php:561
msgid "Minimum bearing"
msgstr ""

#: maincontent.php:569
msgid "Maximum bearing"
msgstr ""

#: maincontent.php:577
msgid "Minimum satellites"
msgstr ""

#: maincontent.php:585
msgid "Maximum satellites"
msgstr ""

#: maincontent.php:597
msgid "Statistics"
msgstr ""

#: maincontent.php:601
msgid "Show stats"
msgstr ""

#: maincontent.php:611
msgid "Shortcuts"
msgstr ""

#: maincontent.php:613
msgid "Toggle sidebar"
msgstr ""

#: maincontent.php:617
msgid "Documentation"
msgstr ""

#: maincontent.php:625
msgid "Source management"
msgstr ""

#: maincontent.php:639
msgid "Authors"
msgstr ""

