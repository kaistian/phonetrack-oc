OC.L10N.register(
    "phonetrack",
    {
    "PhoneTrack" : "फोनटैक",
    "Zoom on all devices" : "सभी उपकरणों पर ज़ूम करें",
    "Click on the map to move the point, press ESC to cancel" : "बिंदु को हिलाने के लिए मानचित्र पर क्लिक करें, ESC दबाएं रद्द करने के लिए",
    "Reserve device names" : "उपकरणों के नाम आरक्षित करें",
    "Files are created in '{exdir}'" : "फ़ाइलें '{exdir}' में बनाई गयी हैं",
    "Reserve this device name" : "इस उपकरण का नाम आरक्षित करें",
    "Impossible to rename device" : "उपकरण का नाम बदलना असंभव हैं",
    "Delete this device" : "हटाएं इस उपकरण को",
    "Rename this device" : "नाम बदलें इस उपकरण का",
    "Ok" : "ठीक",
    "No filters" : "फ़िल्टर नहीं",
    "device name" : "उपकरण का नाम",
    "About PhoneTrack" : "फोनटैक के बारे में",
    "Session" : "सत्र"
},
"nplurals=2; plural=(n != 1);\nX-Generator: crowdin.com\nX-Crowdin-Project: phonetrack\nX-Crowdin-Language: hi");
